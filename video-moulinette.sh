#! /bin/bash

set -o errexit -o errtrace -o pipefail -o nounset
declare -r SCRIPT_PATH="$0"

source config.sh

die() {
	local -r msg="$1"
	printf "\
%s: [ERROR] $msg
" "$SCRIPT_PATH" >&2
	exit 1
}

usage() {
	printf "\
Usage: %s [OPTION]
Edit a video in order to add a startup logo, a title ...

Options:
  -i, --input=FILE              Set the input video path (${CONFIG[video.wxh]})
  -o, --output=FILE             Set the output video path (${CONFIG[video.wxh]})
      --preview                 Run ffplay on the output (no output file)
  -f, --ffmpeg-codecs           Force ffmpeg to encode using the given options
  -h, --help                    Display this message
      --input-start=TIME        Seek to T seconds in the input file
      --input-stop=TIME         Stop reading the input file at position T
      --headline=SRING          Set a headline
      --title=STRING            Set a title
      --sub-title=STRING        Set a subtitle
" "$SCRIPT_PATH"
}

check_file_opt() {
	local -rn opts_cfo_ref="$1"
	local -r opt="$2"

	if ! [[ -r "${opts_cfo_ref[$opt]}" ]]; then
		die "The $opt file (\"${opts_cfo_ref[$opt]}\") does not exist or permission to read it is not granted"
	fi
}

video_duration() {
	local -r video_file="$1"
	local -rn opts_vdu_ref="$2"

	local vstart="0"
	if [[ -v opts_vdu_ref[input-start] ]]; then
		local -r opt_start="${opts_vdu_ref[input-start]%s}"
		vstart="$opt_start"
	fi

	local vstop=$(\
		ffprobe -v error \
		-select_streams v:0 -show_entries stream=duration \
		-of default=noprint_wrappers=1:nokey=1 \
		"$video_file")

	if [[ -v opts_vdu_ref[input-stop] ]]; then
		local -r opt_stop="${opts_vdu_ref[input-stop]%s}"
		if bc <<< "$vstop < $opt_stop" >& /dev/null; then
			vstop="$opt_stop"
		fi
	fi

	printf "%s" "$(bc <<< "$vstop - $vstart")"
}

ffmpeg_video_cmdline() {
	local -r input="$1"
	local -rn opts_fvc_ref="$2"

	local cmdline=""

	if [[ -v opts_fvc_ref[input-start] ]]; then
		cmdline+="-ss ${opts_fvc_ref[input-start]} "
	fi
	if [[ -v opts_fvc_ref[input-stop] ]]; then
		cmdline+="-to ${opts_fvc_ref[input-stop]} "
	fi
	cmdline+="-i $input"
	printf "%s" "$cmdline"
}

# Create the filter adding a bottom title+subtitle at the beginning of the video
vfilter_bottom() {
	local -n video_pad_ref="$1"
	local -n filters_ref="$2"
	local -r bottom_pad="$3"
	local -rn opts_vbo_ref="$4"

	local vfilter_headline=""
	local vfilter_title=""
	local vfilter_subtitle=""
	local vfilter_fade=""

	if [[ -v opts_vbo_ref["headline"] ]]; then
		vfilter_headline="drawtext=\
x=${CONFIG[headline.pos-x]}:\
y=${CONFIG[headline.pos-y]}:\
fontfile=${CONFIG[headline.fontfile]}:\
fontsize=${CONFIG[headline.fontsize]}:\
fontcolor=${CONFIG[headline.fontcolor]}:\
text='${opts_vbo_ref[headline]}',\
"$'\n'
	fi

	if [[ -v opts_vbo_ref["title"] ]]; then
		vfilter_title="drawtext=\
x=${CONFIG[title.pos-x]}:\
y=${CONFIG[title.pos-y]}:\
fontfile=${CONFIG[title.fontfile]}:\
fontsize=${CONFIG[title.fontsize]}:\
fontcolor=${CONFIG[title.fontcolor]}:\
text='${opts_vbo_ref[title]}',\
"$'\n'
	fi

	if [[ -v opts_vbo_ref["subtitle"] ]]; then
		vfilter_subtitle="drawtext=\
x=${CONFIG[subtitle.pos-x]}:\
y=${CONFIG[subtitle.pos-y]}:\
fontfile=${CONFIG[subtitle.fontfile]}:\
fontsize=${CONFIG[subtitle.fontsize]}:\
fontcolor=${CONFIG[subtitle.fontcolor]}:\
text='${opts_vbo_ref[subtitle]}',\
"$'\n'
	fi

	vfilter_fade+="fade=\
type=out:\
start_time=${CONFIG[bottom.duration]}:\
duration=${CONFIG[bottom.transition]}:\
alpha=1"
	filters_ref+=("\
[${bottom_pad}] ${vfilter_headline} ${vfilter_title} ${vfilter_subtitle} ${vfilter_fade} [fade_bottom:v];
[${video_pad_ref}][fade_bottom:v] overlay=shortest=1 [vfilter_bottom:v]")
	video_pad_ref="vfilter_bottom:v"
}

vfilter_startup() {
	local -n video_pad_ref="$1"
	local -n filters_ref="$2"
	local -r startup_pad="$3"
	local -rn opts_vst_ref="$4"

	# Create a filter shifting the main video in order to display a full
	# screen logo at startup
	filters_ref+=("\
color=color=black:\size=${CONFIG[video.wxh]}:duration=${CONFIG[startup.duration]} [color_black:v];
[color_black:v][${video_pad_ref}] concat=n=2:v=1:a=0 [vfilter_shift:v]")
	video_pad_ref="vfilter_shift:v"

	# Create a filter adding the full screen logo at startup
	filters_ref+=("\
[${startup_pad}] fade=\
type=out:\
start_time=${CONFIG[startup.duration]}:\
duration=${CONFIG[startup.transition]}:\
alpha=1 [fade_startup:v];
[${video_pad_ref}][fade_startup:v] overlay=shortest=1 [vfilter_startup:v]")
	video_pad_ref="vfilter_startup:v"
}

afilter_startup() {
	local -n audio_pad_ref="$1"
	local -n filters_ref="$2"
	local -rn opts_ast_ref="$3"

	# Create an audio filter to shift sound in order to display the full
	# screen logo at startup
	filters_ref+=("\
anullsrc=channel_layout=mono:sample_rate=48000 [anullsrc:a];
[anullsrc:a] atrim=duration=${CONFIG[startup.duration]} [anullsrc+atrim:a];
[${audio_pad_ref}] afade=\
type=in:\
start_time=0s:\
duration=${CONFIG[startup.transition]} [afade:a];
[anullsrc+atrim:a][afade:a] concat=n=2:v=0:a=1 [afilter_shift:a]")
	audio_pad_ref="afilter_shift:a"
}

afilter_sjingle() {
	local -n audio_pad_ref="$1"
	local -n filters_ref="$2"
	local -r sjingle_pad="$3"
	local -rn opts_ast_ref="$4"

	# Create an audio filter to shift sound in order to display the full
	# screen logo at startup
	filters_ref+=("\
[${sjingle_pad}] atrim=\
duration=$(bc <<< "${CONFIG[startup.duration]%s} + ${CONFIG[startup.transition]%s}")s [sjingle_atrim:a];
[sjingle_atrim:a][${audio_pad_ref}] acrossfade=\
duration=${CONFIG[startup.transition]}:\
curve1=hsin:\
curve2=hsin [afilter_sjingle:a]")
	audio_pad_ref="afilter_sjingle:a"
}

vfilter_ending() {
	local -n video_pad_ref="$1"
	local -n filters_ref="$2"
	local -r ending_pad="$3"
	local -rn opts_vst_ref="$4"

	# Create a filter extending the main video in order to display a full
	# screen logo at ending
	filters_ref+=("\
color=color=black:\size=${CONFIG[video.wxh]}:duration=${CONFIG[ending.duration]} [color_black:v];
[${video_pad_ref}][color_black:v] concat=n=2:v=1:a=0 [vfilter_extend:v]")
	video_pad_ref="vfilter_extend:v"

	# Create a filter adding the full screen logo at startup
	filters_ref+=("\
[${ending_pad}] fade=\
type=in:\
start_time=$(bc <<< "${CONFIG[startup.duration]%s} + \
                     ${opts_vst_ref[video.duration]} - \
                     ${CONFIG[ending.transition]%s}")s:\
duration=${CONFIG[ending.transition]}:\
alpha=1 [fade_ending:v];
[${video_pad_ref}][fade_ending:v] overlay=shortest=1 [vfilter_ending:v]")
	video_pad_ref="vfilter_ending:v"
}

afilter_ending() {
	local -n audio_pad_ref="$1"
	local -n filters_ref="$2"
	local -r jingle_pad="$3"
	local -rn opts_ast_ref="$4"

	# Create an audio filter to extend sound in order to display the full
	# screen logo at ending
	filters_ref+=("\
anullsrc=channel_layout=mono:sample_rate=48000 [anullsrc:a];
[anullsrc:a] atrim=duration=${CONFIG[ending.duration]} [anullsrc+atrim:a];
[${audio_pad_ref}] afade=\
type=out:\
start_time=$(bc <<< "${CONFIG[startup.duration]%s} + \
                     ${opts_ast_ref[video.duration]} - \
                     ${CONFIG[ending.transition]%s}")s:\
duration=${CONFIG[ending.transition]} [afade:a];
[afade:a][anullsrc+atrim:a] concat=n=2:v=0:a=1 [afilter_extend:a]")
	audio_pad_ref="afilter_extend:a"
}

convert() {
	local -r output_file="$1"
	local -r input_file="$2"
	local -rn opts_ref="$3"

	local -a filters=()
	local -a inputs_cmdline=()

	local video_pad="0:v"
	local audio_pad="0:a"
	local -i startup_pad=0
	local -i sjingle_pad=0
	local -i ending_pad=0
	local -i bottom_pad=0
	local -i current_pad=1

	inputs_cmdline+=("$(ffmpeg_video_cmdline "$input_file" "opts_ref")")

	if [[ -v CONFIG["startup.image"] ]]; then
		check_file_opt "CONFIG" "startup.image"
		startup_pad=$((current_pad++))
		inputs_cmdline+=("-loop 1 -i ${CONFIG[startup.image]}")
	fi

	if [[ -v CONFIG["startup.jingle"] ]]; then
		check_file_opt "CONFIG" "startup.jingle"
		sjingle_pad=$((current_pad++))
		inputs_cmdline+=("-i ${CONFIG[startup.jingle]}")
	fi

	if [[ -v CONFIG["ending.image"] ]]; then
		check_file_opt "CONFIG" "ending.image"
		ending_pad=$((current_pad++))
		inputs_cmdline+=("-loop 1 -i ${CONFIG[ending.image]}")
	fi

	if [[ -v CONFIG["bottom.image"] ]]; then
		check_file_opt "CONFIG" "bottom.image"
		bottom_pad=$((current_pad++))
		inputs_cmdline+=("-loop 1 -i ${CONFIG[bottom.image]}")
	fi

	vfilter_bottom "video_pad" "filters" "$bottom_pad" "opts_ref"

	if [[ -v CONFIG["startup.image"] ]]; then
		vfilter_startup "video_pad" "filters" "$startup_pad" "opts_ref"
		if [[ -v CONFIG["startup.jingle"] ]]; then
			afilter_sjingle "audio_pad" "filters" "$sjingle_pad" "opts_ref"
		else
			afilter_startup "audio_pad" "filters" "opts_ref"
		fi
	fi

	if [[ -v CONFIG["ending.image"] ]]; then
		vfilter_ending "video_pad" "filters" "$ending_pad" "opts_ref"
		afilter_ending "audio_pad" "filters" "FIXME" "opts_ref"
	fi

	local -r inputs_cmdline_list="$(printf "%s " "${inputs_cmdline[@]}")"
	local -r filters_list="
$(printf "%s;"$'\n\n' "${filters[@]::${#filters[@]}-1}")
$(printf $'\n'"%s"$'\n' "${filters[-1]}")
"
	if [[ -z "$output" ]]; then
		set -x
		ffmpeg $inputs_cmdline_list \
		       -filter_complex "$filters_list" \
		       -map "[$video_pad]" \
		       -map "[$audio_pad]" \
		       -f mpeg - | ffplay -
		set +x
	else
		set -x
		ffmpeg $inputs_cmdline_list \
		       -filter_complex "$filters_list" \
		       -map "[$video_pad]" \
		       -map "[$audio_pad]" \
		       ${opts_ref["video.ffmpeg.codecs"]} \
		       "$output"
		set +x
	fi
}

main() {
	local input=""
	local output=""
	local preview="false"
	local -A opts=()

	if ! [[ -x "$(command -v bc)" ]]; then
		die "This program requires you to install bc."
	fi

	TEMP=$(getopt --options "i:o:f:h" \
	              --longoptions "\
input:,input-start:,input-stop:,\
output:,\
preview,\
ffmpeg-codecs,\
headline:,title:,subtitle:,\
help" -- "$@")
	eval set -- "$TEMP"

	opts["video.ffmpeg.codecs"]="${CONFIG[video.ffmpeg.codecs]}"
	while true; do
		case "$1" in
			-i|--input)
				input="$2"
				shift
				;;
			-o|--output)
				output="$2"
				shift
				;;
			--preview)
				preview="true"
				;;
			-f|--ffmpeg-codecs)
				opts["video.ffmpeg.codecs"]="$2"
				shift
				;;
			-h|--help)
				usage
				exit 0
				;;
			--)
				break
				;;
			*)
				opts[${1##--}]="$2"
				shift
				;;
		esac
		shift
	done

	for var in "input"; do
		local -n v="$var"
		if [[ -z "$v" ]]; then
			die "$var option is mandatory"
		fi
	done
	if ! [[ -r "$input" ]]; then
		die "The input file (\"$input\") does not exist or permission to read it is not granted"
	fi
	if [[ -n "$output" ]] && [[ -e "$output" ]]; then
		die "The output file (\"$output\") already exists"
	fi
	if $preview && [[ -n "$output" ]]; then
		die "These two options can't be both specified: --output and --preview"
	fi
	if !( $preview || [[ -n "$output" ]] ); then
		die "At least one of these two options must be specified: --output and --preview"
	fi

	opts["video.duration"]="$(video_duration "$input" "opts")"
	convert "$output" "$input" "opts"
}

main "$@"
