declare -rA CONFIG=(
	["video.wxh"]="1920x1080"
	["video.ffmpeg.codecs"]="-c:v libx264 -preset slow -crf 20 -c:a aac"
	["video.duration"]="RESERVED"

	["startup.image"]="FILE"
	["startup.jingle"]="FILE"
	["startup.duration"]="3.0s"
	["startup.transition"]="1.5s"

	["ending.image"]="FILE"
	["ending.duration"]="3.0s"
	["ending.transition"]="1.5s"

	["bottom.image"]="FILE with transparent layer"
	["bottom.transition"]="1.5s"
	["bottom.duration"]="5s"

	["headline.pos-x"]="450"
	["headline.pos-y"]="(main_h-180)"
	["headline.fontfile"]="/usr/share/fonts/TTF/DejaVuSans-Bold.ttf"
	["headline.fontsize"]="30"
	["headline.fontcolor"]="0xee3876"

	["title.pos-x"]="500"
	["title.pos-y"]="(main_h-130)"
	["title.fontfile"]="/usr/share/fonts/TTF/DejaVuSans-Bold.ttf"
	["title.fontsize"]="30"
	["title.fontcolor"]="white"

	["subtitle.pos-x"]="550"
	["subtitle.pos-y"]="(main_h-80)"
	["subtitle.fontfile"]="/usr/share/fonts/TTF/DejaVuSans-Bold.ttf"
	["subtitle.fontsize"]="30"
	["subtitle.fontcolor"]="white"
)
